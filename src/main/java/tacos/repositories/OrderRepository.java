package tacos.repositories;

import org.springframework.data.repository.CrudRepository;
import tacos.model.Order;

import java.util.Date;
import java.util.List;

/**
 * IsAfter, After, IsGreaterThan, GreaterTh
 * IsGreaterThanEqual, GreaterThanEqual
 * IsBefore, Before, IsLessThan, LessThan
 * IsLessThanEqual, LessThanEqual
 * IsBetween, Between
 * IsNull, Null
 * IsNotNull, NotNull
 * IsIn, In
 * IsNotIn, NotIn
 * IsStartingWith, StartingWith, StartsWith
 * IsEndingWith, EndingWith, EndsWith
 * IsContaining, Containing, Contains
 * IsLike, Like
 * IsNotLike, NotLike
 * IsTrue, True
 * IsFalse, False
 * Is, Equals
 * IsNot, Not
 * IgnoringCase, IgnoresCase
 *
 * When generating the repository implementation, Spring Data examines any methods
 * in the repository interface, parses the method name, and attempts to understand the
 * method’s purpose in the context of the persisted object (an Order, in this case). In
 * essence, Spring Data defines a sort of miniature domain-specific language (DSL)
 * where persistence details are expressed in repository method signatures.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findAll();

    List<Order> findByDeliveryZip(String deliveryZip);


    /**
     * Spring Data also understands find, read, and get as synonymous for fetching one
     * or more entities
     *
     * Although the subject of the method is optional, here it says Orders. Spring Data
     * ignores most words in a subject, so you could name the method readPuppiesBy...
     * and it would still find Order entities, as that is the type that CrudRepository is parameterized with
     */
    List<Order> readOrdersByDeliveryZipAndPlacedAtBetween(String deliveryZip, Date startDate, Date endDate);

    // TODO how query dsl working???
//    @Query("Order o where o.delivery_city='Seattle'")
//    List<Order> readOrdersDeliveredInSeattle();

}
