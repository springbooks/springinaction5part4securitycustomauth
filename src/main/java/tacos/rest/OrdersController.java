package tacos.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tacos.model.Order;
import tacos.repositories.OrderRepository;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrdersController {

    private final OrderRepository orderRepo;

    public OrdersController(OrderRepository orderRepo) {
        this.orderRepo = orderRepo;
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> findAllOrders() {
        List<Order> allOrders = orderRepo.findAll();
        return new ResponseEntity<>(allOrders, HttpStatus.OK);
    }

}
