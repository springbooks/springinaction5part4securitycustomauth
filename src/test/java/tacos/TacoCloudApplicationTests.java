package tacos;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

@SpringBootTest
class TacoCloudApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void encodePassword() {
		String encoded = new StandardPasswordEncoder("53cr3t").encode("test");
		System.out.println(encoded);
	}

}
